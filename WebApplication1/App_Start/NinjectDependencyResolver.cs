﻿using Templete.Data.EntityFramework.Concrete.UnitOfWorks;
using Template.Domain.Abstract.UnitOfWorks;
using Templete.Identity.Identities;
using Templete.Identity.Managers;
using Templete.Identity.Stores;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.DataProtection;
using Ninject;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using Microsoft.Owin;
using System.Web;



namespace Template.WebUI.App_Start
{
    class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IAppBuilder app)
        {
            kernel = new StandardKernel();
            AddBinding(app);
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }

        private void AddBinding(IAppBuilder app)
        {
            //Binding
            kernel.Bind<IUnitOfWork>().To<UnitOfWork>();

            #region Identity Managers and AuthenticationManager
            kernel.Bind<IUserStore<IdentityUser, int>>().To<CustomUserStore>();

            kernel.Bind<UserManager<IdentityUser, int>>()
                  .To<ApplicationUserManager>()
                  .WithConstructorArgument<IDataProtectionProvider>(app.GetDataProtectionProvider());

            kernel.Bind<IAuthenticationManager>().ToMethod(c => HttpContext.Current.GetOwinContext().Authentication);
            #endregion

            
        }
    }
}
