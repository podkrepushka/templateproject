﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Templete.Services.Infrastructure.Validation;

namespace Template.WebUI.Infrastructure.Validation
{
    public class ModelStateWrapper : IValidationDictionary
    {
        private ModelStateDictionary ModelState;

        public ModelStateWrapper(ModelStateDictionary modelState)
        {
            ModelState = modelState;
        }

        public void AddError(string key, string errorMessage)
        {
            ModelState.AddModelError(key, errorMessage);
        }

        public bool IsValid
        {
            get 
            {
                return ModelState.IsValid;
            }
        }
    }
}
