﻿using Microsoft.Owin;
using Owin;
using System.Web.Mvc;
using Template.WebUI.App_Start;

[assembly: OwinStartupAttribute(typeof(Template.WebUI.Startup))]
namespace Template.WebUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            DependencyResolver.SetResolver(new NinjectDependencyResolver(app));
        }
    }
}
