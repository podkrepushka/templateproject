﻿using Template.Domain.Abstract.UnitOfWorks;
using Templete.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Templete.Services.Infrastructure.Validation;

namespace Templete.Services.Concrete
{
    public class Service : IService
    {
        private IValidationDictionary ModelState;
        private IUnitOfWork UnitOfWork;

        public Service(IUnitOfWork unitOfWork, IValidationDictionary modelState)
        {
            ModelState = modelState;
            UnitOfWork = unitOfWork;
        }
    }
}
