﻿using Templete.Data.EntityFramework.Concrete.Repositories;
using Templete.Data.EntityFramework.DBContext;
using Template.Domain.Abstract.Repositories;
using Template.Domain.Abstract.UnitOfWorks;
using System.Threading.Tasks;
namespace Templete.Data.EntityFramework.Concrete.UnitOfWorks
{
    public class UnitOfWork : IUnitOfWork
    {
        #region Fields
        private readonly ApplicationDbContext Context;
        private IExternalLoginRepository externalLoginRepository;
        private IRoleRepository roleRepository;
        private IUserRepository userRepository;
        #endregion

        #region Constructors
        public UnitOfWork()//string nameOrConnectionString)
        {
            Context = new ApplicationDbContext();//nameOrConnectionString);
        }
        #endregion

        #region IUnitOfWork Members
        public IExternalLoginRepository ExternalLoginRepository
        {
            get { return externalLoginRepository ?? (externalLoginRepository = new ExternalLoginRepository(Context)); }
        }

        public IRoleRepository RoleRepository
        {
            get { return roleRepository ?? (roleRepository = new RoleRepository(Context)); }
        }

        public IUserRepository UserRepository
        {
            get { return userRepository ?? (userRepository = new UserRepository(Context)); }
        }

        public int SaveChanges()
        {
            return Context.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return Context.SaveChangesAsync();
        }

        public Task<int> SaveChangesAsync(System.Threading.CancellationToken cancellationToken)
        {
            return Context.SaveChangesAsync(cancellationToken);
        }
        #endregion

        #region IDisposable Members
        public void Dispose()
        {
            externalLoginRepository = null;
            roleRepository = null;
            userRepository = null;
            Context.Dispose();
        }
        #endregion
    }
}