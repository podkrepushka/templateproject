﻿using Templete.Identity.Identities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;


namespace Templete.Identity.Managers
{
    public class ApplicationSignInManager: SignInManager<IdentityUser, int>
    {
        public ApplicationSignInManager(UserManager<IdentityUser,int> userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(IdentityUser user)
        {
            return user.GenerateUserIdentityAsync(UserManager);
        }

    }
}
