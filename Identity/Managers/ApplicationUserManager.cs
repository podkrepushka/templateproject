﻿
using Template.Domain.Abstract.UnitOfWorks;
using Templete.Identity.Identities;
using Templete.Identity.Service;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.DataProtection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


namespace Templete.Identity.Managers
{
    // Настройка диспетчера пользователей приложения. UserManager определяется в ASP.NET Identity и используется приложением.
    public class ApplicationUserManager : UserManager<IdentityUser, int>
    {

        public ApplicationUserManager(IUserStore<IdentityUser, int> store, IDataProtectionProvider dataProtectionProvider)//,IdentityFactoryOptions<ApplicationUserManager> options)
            : base(store)
        {
            ///
            /// Настройка UserManager
            ///
            this.UserValidator = new UserValidator<IdentityUser, int>(this)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Настройка логики проверки паролей
            this.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };

            // Настройка параметров блокировки по умолчанию
            this.UserLockoutEnabledByDefault = false;//
            this.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            this.MaxFailedAccessAttemptsBeforeLockout = 5;

            // Регистрация поставщиков двухфакторной проверки подлинности. Для получения кода проверки пользователя в данном приложении используется телефон и сообщения электронной почты
            // Здесь можно указать собственный поставщик и подключить его.
            this.RegisterTwoFactorProvider("Код, полученный по телефону", new PhoneNumberTokenProvider<IdentityUser, int>
            {
                MessageFormat = "Ваш код безопасности: {0}"
            });
            this.RegisterTwoFactorProvider("Код из сообщения", new EmailTokenProvider<IdentityUser, int>
            {
                Subject = "Код безопасности",
                BodyFormat = "Ваш код безопасности: {0}"
            });
            this.EmailService = new EmailService();
            this.SmsService = new SmsService();
            //var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                this.UserTokenProvider =
                    new DataProtectorTokenProvider<IdentityUser, int>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
        }

    }
}
