﻿using System.Collections.Generic;

namespace Template.Domain.Entities.Identity
{
    public class User
    {

        #region Properties
        public virtual int Id { get; set; }
        public virtual string UserName { get; set; }
        public virtual string Email { get; set; }
        public virtual bool EmailConfirmed { get; set; }
        public virtual string PasswordHash { get; set; }
        public virtual string SecurityStamp { get; set; }
        #endregion

        #region Navigation Properties
        public virtual ICollection<Claim> Claims { get; set; }
        public virtual ICollection<ExternalLogin> ExternalLogins { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
        #endregion

    }
}
