﻿
namespace Template.Domain.Entities.Identity
{
    public class ExternalLogin
    {
        #region Properties
        public virtual string LoginProvider { get; set; }
        public virtual string ProviderKey { get; set; }
        public virtual int UserId { get; set; }
        #endregion

        #region Navigation Properties
        public virtual User User { get; set; }
        #endregion
    }
}
